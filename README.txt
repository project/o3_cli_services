CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Usage
 * Maintainers


INTRODUCTION
------------

O3 CLI Services integrates any Drupal site with the O3 CLI
(https://www.npmjs.com/package/o3-cli) tool.

In its current version, O3 CLI Services exposes two API endpoints to empower
developers and QA engineers to query path aliases of nodes by content types,
paragraph types, and menus.


REQUIREMENTS
------------

Download the O3 CLI (https://www.npmjs.com/package/o3-cli) by running:

npm install -g o3-cli



RECOMMENDED MODULES
-------------------

No recommended modules.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration for this module.


USAGE
-----

Use the /o3-cli-api/url-sources API endpoint to get a list of sources of path
aliases, and use the /o3-cli-api/urls API to get a list of path aliases, using
your sources as filters.

Souce filters for /o3-cli-api/urls:
  - content_types
    - Include a comma-separated list of content type machine names.
  - paragraph_types
    - Include a comma-separated list of content type machine names.
  - menus
    - Include a comma-separated list of menu machine names.
  - limit
    - Include an integer limit to control the maximum number of path aliases to
      return for each machine name in any source.

An example request:

GET http://example.com/o3-cli-api/urls?content_types=article,blog&paragraph_types=hero&menus=primary,footer&limit=10

The above example requests the path aliases of content types with the machine
names of 'article' or 'blog', paragraphs with the machine name of 'hero', menus
with machine names 'primary' or 'footer', and a maximum of 10 results per
content type and paragraph type.

The O3 CLI automatically generates requests like the above, and it empowers
developers and QA engineers to dynamically generate visual regression tests,
among other needs. See the documentation at
https://www.npmjs.com/package/o3-cli.

MAINTAINERS
-----------

Current maintainers:
 * Matt Schaff (gwolfman) - https://www.drupal.org/u/gwolfman

This project has been sponsored by:
 * O3 World
   O3 World is a digital agency in Philadelphia that builds great products,
   transforms digital experiences, and takes your most innovative ideas to
   market.
