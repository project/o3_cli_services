<?php

namespace Drupal\o3_cli_services\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\o3_cli_services\Service\urlListManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for the /o3-cli-api/url-sources API endpoint
 *
 * - Exposes via an API the sources of URLs across the system, with respect to
 *   node types, paragraph types, and menus
 */
class UrlSourcesController extends ControllerBase {

  /**
   * Generic test generator service
   *
   * @var UrlListManager
   */
  protected $urlListManager;

  /**
   * Constructs a new UrlListController object.
   */
  public function __construct(UrlListManager $UrlListManager) {
    $this->urlListManager = $UrlListManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('o3_cli_services.url_list_manager')
    );
  }

  /**
   * Callback for /o3-cli-api/url-sources
   *
   * @return CacheableJsonResponse
   */
  public function getSources() {
    $return_array = [
      'content_types' => $this->urlListManager->countContentTypeNodes(),
      'menus' => $this->urlListManager->countMenuNodes(),
    ];
    $paragraph_types_data = $this->urlListManager->countParagraphNodes();
    if (count($paragraph_types_data)) {
      $return_array['paragraph_types'] = $paragraph_types_data;
    }
    return new CacheableJsonResponse($return_array);
  }

}
