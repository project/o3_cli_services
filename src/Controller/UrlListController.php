<?php

namespace Drupal\o3_cli_services\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\o3_cli_services\Model\O3UrlListPlan;
use Drupal\o3_cli_services\Service\UrlListManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for the /o3-cli-api/urls API endpoint
 */
class UrlListController extends ControllerBase {

  /**
   * Generic test generator service
   *
   * @var UrlListManager
   */
  protected $urlListManager;

  /**
   * Dependency Injection Container
   *
   * @var ContainerInterface
   */
  protected $dependencyInjectionContainer;

  /**
   * Constructs a new UrlListController object.
   *
   * @param UrlListManager $UrlListManager
   * @param ContainerInterface $Container
   */
  public function __construct(UrlListManager $UrlListManager, ContainerInterface $Container) {
    $this->urlListManager = $UrlListManager;
    $this->dependencyInjectionContainer = $Container;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('o3_cli_services.url_list_manager'),
      $container
    );
  }

  /**
   * Callback for /o3-cli-api/urls
   *
   * @return CacheableJsonResponse
   */
  public function getUrls(Request $request) {
    // Return OK response only if correct query parameters are provided.
    $response = new CacheableJsonResponse([]);
    $response->getCacheableMetadata()->addCacheContexts(['url.query_args']);
    if ($test_plan = $this->getUrlListPlan($request)) {
      $response_array = $this->urlListManager->buildTestArray($test_plan, $response);
      $http_code = Response::HTTP_OK;
    }
    else {
      $response_array = [
        'success' => false,
        'message' => $this->t('The generic test generator requires either valid query parameters for either \'content_types\' or \'menus\'.'),
      ];
      $http_code = Response::HTTP_BAD_REQUEST;
    }
    $response->setData($response_array);
    return $response->setStatusCode($http_code);
  }

  /**
   * Parses parameters in the Request query string
   *
   * @param Request $request
   * @return O3UrlListPlan|false
   */
  protected function getUrlListPlan($request) {
    if ($parameters_array = $request->query->all()) {
      $test_plan = O3UrlListPlan::create($parameters_array, $this->dependencyInjectionContainer);
      if ($test_plan->isPlanValid()) {
        return $test_plan;
      }
    }
    return FALSE;
  }
}
