<?php

namespace Drupal\o3_cli_services\Service;

use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\o3_cli_services\UrlListPlanInterface;

/**
 * A generic service that returns paths of nodes and menu items based on HTTP
 * request query parameters
 */
class UrlListManager {

  /**
   * A list of menus that cause cache metatata leaks when counting
   */
  const MENUS_TO_NOT_COUNT = [
    'devel'
  ];

  /**
   * Entity type manager service
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Menu link tree service
   *
   * @var MenuLinkTreeInterface
   */
  protected $menuLinkTree;

  /**
   * Database service
   *
   * @var Connection
   */
  protected $database;

  /**
   * Database service
   *
   * @var ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs the GenericTestGenerator object
   *
   * @param EntityTypeManagerInterface $EntityTypeManager
   * @param MenuLinkTreeInterface $MenuLinkTree
   * @param Connection $Connection
   * @param ModuleHandlerInterface $ModuleHandler
   */
  public function __construct(EntityTypeManagerInterface $EntityTypeManager, MenuLinkTreeInterface $MenuLinkTree, Connection $Connection, ModuleHandlerInterface $ModuleHandler) {
    $this->entityTypeManager = $EntityTypeManager;
    $this->menuLinkTree = $MenuLinkTree;
    $this->database = $Connection;
    $this->moduleHandler = $ModuleHandler;
  }

  /**
   * Build generic test array, to be converted to JSON
   *
   * @param UrlListPlanInterface $test_plan
   * @param CacheableResponseInterface|null
   * @return array
   */
  public function buildTestArray(UrlListPlanInterface $test_plan, CacheableResponseInterface $response = NULL) {
    $node_paths = $this->buildNodePaths($test_plan, $response);
    $paragraph_paths = $this->buildParagraphPaths($test_plan, $response);
    $menu_paths = $this->buildMenuPaths($test_plan, $response);
    $merged_paths = array_merge($node_paths, $paragraph_paths, $menu_paths);
    sort($merged_paths);
    return $merged_paths;
  }

  /**
   * Build the node paths from the provided test plan
   *
   * @param UrlListPlanInterface $test_plan
   * @param CacheableResponseInterface $response
   * @return array $paths
   */
  protected function buildNodePaths(UrlListPlanInterface $test_plan, CacheableResponseInterface $response = NULL) {
    // Get the nodes, limit by limit.
    $properties = $test_plan->getProperties();
    $nids = $this->getContentTypeNids('path', $properties['content_types'], $properties['limit']);
    $paths = [];
    foreach ($this->entityTypeManager->getStorage('node')->loadMultiple($nids) as $node) {
      if ($response) {
        $response->addCacheableDependency($node);
      }
      $paths[] = $node->toUrl()->toString(TRUE)->getGeneratedUrl();
    }
    return $paths;
  }

  /**
   * Get content types, with count of each
   *
   * @return array
   */
  public function countContentTypeNodes() {
    $content_types = array_keys($this->entityTypeManager->getStorage('node_type')->loadMultiple());
    sort($content_types);
    return $this->getContentTypeNids('count', $content_types);
  }

  /**
   * Get content type NIDs, depending on count or path
   *
   * @param string $data_type
   * @param array $content_types
   * @param boolean $limit
   * @return void
   */
  protected function getContentTypeNids($data_type = 'path', $content_types, $limit = FALSE) {
    $extracted_data = [];
    foreach ($content_types as $content_type) {
      $query = $this->entityTypeManager->getStorage('node')->getQuery();
      $query->sort('created' , 'DESC')
        ->condition('type', $content_type);
      if ($limit !== FALSE && $limit > 0) {
        $query->range(0, $limit);
      }
      if ($data_type === 'path') {
        $extracted_data = array_merge($extracted_data, array_values($query->execute()));
      }
      elseif (($data_type === 'count') && ($count = (int) $query->count()->execute())) {
        $extracted_data[$content_type] = ['count' => $count];
      }

    }
    return $extracted_data;
  }

  /**
   * Build the node paths from the provided test plan
   *
   * @param UrlListPlanInterface $test_plan
   * @param CacheableResponseInterface $response
   * @return array $paths
   */
  protected function buildParagraphPaths(UrlListPlanInterface $test_plan, CacheableResponseInterface $response = NULL) {
    $properties = $test_plan->getProperties();
    // Make sure paragraphs module exists.
    if (!$this->moduleHandler->moduleExists('paragraphs') || !isset($properties['paragraph_types'])) {
      return [];
    }
    $extracted_nids = $this->getParagraphNids('path', $properties['paragraph_types'], $properties['limit']);
    // Load nodes to get path aliases.
    $paths = [];
    foreach ($this->entityTypeManager->getStorage('node')->loadMultiple($extracted_nids) as $node) {
      if ($response) {
        $response->addCacheableDependency($node);
      }
      $paths[] = $node->toUrl()->toString(TRUE)->getGeneratedUrl();
    }
    return $paths;
  }

  /**
   * Count paragraph nodes
   *
   * @return array
   */
  public function countParagraphNodes() {
    if (!$this->moduleHandler->moduleExists('paragraphs')) {
      return [];
    }
    return $this->getParagraphNids('count');
  }

  /**
   * Get paragraph NIDs
   *
   * @param string $data_type
   * @param array $paragraph_types
   * @param boolean $limit
   * @return array $extracted_data
   */
  protected function getParagraphNids($data_type = 'path', $paragraph_types = NULL, $limit = FALSE) {
    // Get node IDs that have paragraphs.
    $query = $this->database->select('paragraphs_item_field_data', 'p')
      ->fields('p', ['type', 'parent_id'])
      ->condition('p.parent_type', 'node')
      ->orderBy('p.type');
    if ($paragraph_types) {
      $query->condition('p.type', $paragraph_types, 'IN');
    }
    // Group node IDs by paragraph type.
    $paragraph_nids = [];
    foreach ($query->execute()->fetchAll() as $paragraph_item) {
      $paragraph_nids[$paragraph_item->type][$paragraph_item->parent_id] = $paragraph_item->parent_id;
    }
    // Use entity query to ensure access check.
    $extracted_data = [];
    foreach ($paragraph_nids as $type => $nids) {
      $query = $this->entityTypeManager->getStorage('node')->getQuery();
      $query->sort('created' , 'DESC')
        ->condition('nid', $nids, 'IN')
        ->condition('status', 1);
      if ($limit !== FALSE && $limit > 0) {
        $query->range(0, $limit);
      }
      if ($data_type === 'path') {
        $extracted_data = array_merge($extracted_data, array_values($query->execute()));
      }
      elseif (($data_type === 'count') && ($count = (int) $query->count()->execute())) {
        $extracted_data[$type] = ['count' => $count];
      }
    }
    return $extracted_data;
  }

  /**
   * Build the node paths from the provided test plan
   *
   * @param UrlListPlanInterface $test_plan
   * @return array $paths
   */
  protected function buildMenuPaths(UrlListPlanInterface $test_plan) {
    // Get the nodes, limit by limit.
    $properties = $test_plan->getProperties();
    return isset($properties['menus']) ? $this->getMenuData($properties['menus']) : [];
  }

  /**
   * Count nodes in menus
   *
   * @return array
   */
  public function countMenuNodes() {
    // Use DB select because entityQuery causes cache metadata leaks.
    $menus = $this->database
      ->select('menu_tree', 'm')
      ->distinct()
      ->fields('m', ['menu_name'])
      ->condition('m.menu_name', self::MENUS_TO_NOT_COUNT, 'NOT IN')
      ->orderBy('m.menu_name')
      ->execute()
      ->fetchAllKeyed(0,0);
    return $this->getMenuData($menus, 'count');
  }

  /**
   * Get node either count or path data from menus
   *
   * @param string $menus
   * @param string $data_type
   *   - Either 'path' or 'count'
   * @return array
   */
  protected function getMenuData($menus, $data_type = 'path') {
    $data = [];
    if (!empty($menus)) {
      // Load each menu & extract all child links, recursively.
      foreach ($menus as $menu) {
        if ($menu_tree = $this->menuLinkTree->load($menu, new MenuTreeParameters())) {
          $this->extractPathsFromMenu($data, $menu, $menu_tree, $data_type);
          $data = array_filter(array_unique($data));
        }
      }
    }
    return $data;
  }

  /**
   * A recursive function that extracts link paths in nested menus
   *s
   * - Only selects links with internal URLs
   *
   * @param array $paths
   * @param string $menu
   * @param array $menu_tree
   * @param string $data_type
   *   - Either 'path' or 'count'
   */
  protected function extractPathsFromMenu(array &$data, string $menu, array $menu_tree, string $data_type) {
    foreach (array_values($menu_tree) as $item) {
      if ($item->link->isEnabled() && !$item->link->getUrlObject()->isExternal()) {
        if ($data_type === 'path') {
          $data[] = $item->link->getUrlObject()->toString(TRUE)->getGeneratedUrl();
        }
        elseif ($data_type === 'count') {
          $data[$menu] = isset($data[$menu]) ? $data[$menu] + 1 : 1;
        }
        if ($item->hasChildren) {
          $this->extractPathsFromMenu($data, $menu, $item->subtree, $data_type);
        }
      }
    }
  }

}
