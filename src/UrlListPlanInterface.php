<?php

namespace Drupal\o3_cli_services;

use Symfony\Component\DependencyInjection\ContainerInterface;

interface UrlListPlanInterface {
  /**
   * Checks if the plan is valid
   *
   * @return boolean
   */
  public function isPlanValid();

  /**
   * Get properties, keyed by property name
   *
   * @return array
   */
  public function getProperties();

  /**
   * Static create function
   *
   * @param array $parameters
   * @param ContainerInterface $container
   * @return UrlListPlanInterface
   */
  public static function create(array $parameters, ContainerInterface $container);
}
