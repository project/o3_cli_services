<?php

namespace Drupal\o3_cli_services\Model;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A URL list plant with queried content types, max node limit and menu names
 * for which paths will be generated
 */
class O3UrlListPlan extends AbstractUrlListPlan {

  /**
   * Entity type manager service
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Content types
   *
   * @var array
   */
  protected $contentTypes;

  /**
   * Paragraph types
   *
   * @var array
   */
  protected $paragraphTypes;

  /**
   * Menus
   *
   * @var array
   */
  protected $menus;

  /**
   * Number of nodes to include in test
   *
   * @var integer
   */
  protected $limit = 10;

  /**
   * Constructs the O3UrlListPlan object
   *
   * @param array $parameters
   */
  public  function __construct(array $parameters, EntityTypeManagerInterface $EntityTypeManager) {
    $this->entityTypeManager = $EntityTypeManager;
    $this->setProperties($parameters);
  }

  public static function create(array $parameters, ContainerInterface $container) {
    return new static(
      $parameters,
      $container->get('entity_type.manager')
    );
  }

  /**
   * Set properties
   *
   * @param array $parameters
   */
  protected function setProperties($parameters) {
    foreach ($parameters as $key => $value) {
      switch ($key) {
        case 'content_types':
          $this->contentTypes = $this->getValidContentTypes($value);
          break;
        case 'paragraph_types':
            $this->paragraphTypes = $this->getValidParagraphTypes($value);
            break;
        case 'menus':
            $this->menus = $this->getValidMenus($value);
            break;
        case 'limit':
          $this->limit = $value;
          break;
      }
    }
  }

  /**
   * @inheritDoc
   */
  public function getProperties() {
    return [
      'content_types' => $this->contentTypes,
      'paragraph_types' => $this->paragraphTypes,
      'limit' => $this->limit,
      'menus' => $this->menus,
    ];
  }

  /**
   * @inheritDoc
   */
  public function isPlanValid() {
    if (!empty($this->contentTypes) || !empty($this->menus) || !empty($this->paragraphTypes)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get valid content types
   *
   * @param string $string
   * @return array
   */
  protected function getValidContentTypes($string) {
    $content_types = [];
    foreach(explode(',', $string) as $raw_content_type) {
      if ($this->entityTypeManager->getStorage('node_type')->load($raw_content_type)) {
        $content_types[] = $raw_content_type;
      }
    }
    return $content_types;
  }

  /**
   * Get valid paragraph types
   *
   * @param string $string
   * @return array
   */
  protected function getValidParagraphTypes($string) {
    $paragraph_types = [];
    foreach(explode(',', $string) as $raw_paragraph_type) {
      if ($this->entityTypeManager->getStorage('paragraphs_type')->load($raw_paragraph_type)) {
        $paragraph_types[] = $raw_paragraph_type;
      }
    }
    return $paragraph_types;
  }

  /**
   * Get valid menu
   *
   * @param string $string
   * @return array
   */
  protected function getValidMenus($string) {
    $menus = [];
    foreach(explode(',', $string) as $raw_menu) {
      if ($this->entityTypeManager->getStorage('menu_link_content')->loadByProperties(['menu_name' => $raw_menu])) {
        $menus[] = $raw_menu;
      }
    }
    return $menus;
  }
}
