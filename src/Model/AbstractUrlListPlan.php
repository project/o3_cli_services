<?php

namespace Drupal\o3_cli_services\Model;

use Drupal\o3_cli_services\UrlListPlanInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An abstract URL list plan
 */
class AbstractUrlListPlan implements UrlListPlanInterface {

  /**
   * Constructs the AbstractUrlListPlan object
   *
   * @param array $parameters
   */
  public  function __construct(array $parameters) {
  }

  /**
   * @inheritDoc
   */
  public static function create(array $parameters, ContainerInterface $container) {
    return new static(
      $parameters
    );
  }

  /**
   * @inheritDoc
   */
  public function getProperties() {
    return [];
  }

  /**
   * @inheritDoc
   */
  public function isPlanValid() {
    return TRUE;
  }

}
